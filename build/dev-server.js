'use strict'
require('./check-versions')()

const config = require('../config')
if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = JSON.parse(config.dev.env.NODE_ENV)
}

const opn = require('opn')
const path = require('path')
const express = require('express')
const sslRedirect = require('heroku-ssl-redirect')
const webpack = require('webpack')
const proxyMiddleware = require('http-proxy-middleware')
const https = require('https');
const webpackConfig = (process.env.NODE_ENV === 'testing' || process.env.NODE_ENV === 'production')
  ? require('./webpack.prod.conf')
  : require('./webpack.dev.conf')

// default port where dev server listens for incoming traffic
const port = process.env.PORT || config.dev.port
console.log('port');
console.log(port);
// automatically open browser, if not set will be false
const autoOpenBrowser = !!config.dev.autoOpenBrowser
// Define HTTP proxies to your custom API backend
// https://github.com/chimurai/http-proxy-middleware
const proxyTable = config.dev.proxyTable

const app = express()
const DIST_DIR  = path.resolve(__dirname, "../dist/")

if(process.env.NODE_ENV === 'production') {
  console.log('serve dir: ' + DIST_DIR);
  console.log('serve port: ' + port);
  //app.use(sslRedirect());
  app.use(express.static(DIST_DIR));
  app.set('port', port);
  // these need to go first:
  app.use("/js", express.static(DIST_DIR + "/static/js"));
  app.use("/img", express.static(DIST_DIR + "/static/img"));
  app.use("/css", express.static(DIST_DIR + "/static/css"));
  app.get('/*', function(req, res){
    res.sendFile(DIST_DIR + '/index.html');
  });
  app.listen(app.get("port"));
}

if (process.env.NODE_ENV === 'testing' || process.env.NODE_ENV === 'development') {
  console.log('TEST OR DEV');
  const compiler = webpack(webpackConfig)

  const devMiddleware = require('webpack-dev-middleware')(compiler, {
    publicPath: webpackConfig.output.publicPath,
    quiet: false//,
    // proxy: {
    //   changeOrigin: true,
    //   secure: true
    // },
    // https: {
    //       cert: config.dev.credentials.cert,
    //       key: config.dev.credentials.key
    // }
    // https: true
  })

  const hotMiddleware = require('webpack-hot-middleware')(compiler, {
    log: false,
    heartbeat: 2000
  })
  // force page reload when html-webpack-plugin template changes
  // currently disabled until this is resolved:
  // https://github.com/jantimon/html-webpack-plugin/issues/680
  // compiler.plugin('compilation', function (compilation) {
  //   compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
  //     hotMiddleware.publish({ action: 'reload' })
  //     cb()
  //   })
  // })

  // enable hot-reload and state-preserving
  // compilation error display
  app.use(hotMiddleware)

  // proxy api requests
  Object.keys(proxyTable).forEach(function (context) {
    let options = proxyTable[context]
    if (typeof options === 'string') {
      options = { target: options }
    }
    app.use(proxyMiddleware(options.filter || context, options))
  })

  // handle fallback for HTML5 history API
  app.use(require('connect-history-api-fallback')())

  // serve webpack bundle output
  app.use(devMiddleware)

  // serve pure static assets
  const staticPath = path.posix.join(config.dev.assetsPublicPath, config.dev.assetsSubDirectory)
  app.use(staticPath, express.static('./static'))

  const uri = 'https://localhost:' + port

  var _resolve
  var _reject
  var readyPromise = new Promise((resolve, reject) => {
    _resolve = resolve
    _reject = reject
  })

  var server
  var portfinder = require('portfinder')
  portfinder.basePort = port

  console.log('> Starting dev server...')
  devMiddleware.waitUntilValid(() => {
    portfinder.getPort((err, port) => {
      if (err) {
        _reject(err)
      }
      process.env.PORT = port
      if(process.env.NODE_ENV === 'testing' || process.env.NODE_ENV === 'production'){
        var uri = 'http://localhost:' + port
        console.log('Use uri local https');
      } else {
        var uri = 'https://localhost:' + port
        console.log('Use uri local https');
      }

      console.log('> Listening at ' + uri + '\n')
      // when env is testing, don't need open it
      if (autoOpenBrowser && process.env.NODE_ENV !== 'testing') {
        opn(uri)
      }

      if(process.env.NODE_ENV === 'testing' || process.env.NODE_ENV === 'production') {
        //Http
        console.log('HTTP if');
        server = app.listen(port)
      } else {
        //Https
        var httpsServer = https.createServer(config.dev.credentials, app);
        server = httpsServer.listen(port);
        console.log('HTTPs if');
      }

      console.log(process.env.NODE_ENV);
      console.log('The value of PORT is:', process.env.PORT);
      _resolve()
    })
  })

}

module.exports = {
  ready: readyPromise,
  close: () => {
    server.close()
  }
}
