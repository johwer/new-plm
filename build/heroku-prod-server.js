'use strict'
// const fs = require('fs')
const path = require('path')
const express = require('express')
const sslRedirect = require('heroku-ssl-redirect')
const expressStaticGzip = require("express-static-gzip");
// const spdy = require('spdy')
// const config = require('../config')

const port = process.env.PORT // || config.dev.port
const app = express()
const DIST_DIR = path.resolve(__dirname, "../dist/")

console.log('serve dir: ' + DIST_DIR);
console.log('serve port: ' + port);

app.use(sslRedirect());
app.use('/', expressStaticGzip(DIST_DIR));
app.set('port', port);

app.use("/js", express.static(DIST_DIR + "/static/js"));
app.use("/img", express.static(DIST_DIR + "/static/img"));
app.use("/css", express.static(DIST_DIR + "/static/css"));
app.get('/*', function(req, res){
  res.sendFile(DIST_DIR + '/index.html');
});

/*const credentials = {
  cert: fs.readFileSync(path.resolve(__dirname, '../dist/static/cert/cert.pem'), 'utf8'),
  key: fs.readFileSync(path.resolve(__dirname, '../dist/static/cert/key.pem' ), 'utf8'),
  passphrase: fs.readFileSync(path.resolve(__dirname, '../dist/static/cert/passphrase.txt'), 'utf8')
}

console.log('_____CREDENTIALS_________')
console.log(credentials);*/

/*spdy  
  .createServer(credentials, app)
  .listen(port, (err) => {
    if (err) {
      throw new Error(err);
    }

    console.log('Listening on port: ' + port + '.');
  });*/

app.listen(app.get("port"));