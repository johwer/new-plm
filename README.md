# new-plm

> A Vue.js project

##
> Need to create cert and place it in cert folder  
The certification need  
cert.pem  
key.pem   
passphrase.txt (the password as it seems is hard to remove these days)  

To generate key.pem, cert.pem and an empty passphrase.txt:

openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem

touch passphrase.txt


https://github.com/webpack/webpack-dev-server/blob/master/examples/https/README.md  
http://blog.mgechev.com/2014/02/19/create-https-tls-ssl-application-with-express-nodejs/  

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
