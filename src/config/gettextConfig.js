import Vue from 'vue';
import GetTextPlugin from 'vue-gettext';
import translations from '../../static/locale/en/translation.json';

Vue.use(GetTextPlugin, {
  availableLanguages: {
    en_GB: 'British English',
    en_US: 'American English',
    fr_FR: 'Français',
  },
  defaultLanguage: 'en',
  translations,
  silent: true,
});
