export const authorizationUrl = 'https://login-ppr.schneider-electric.com';
export const clientId = 'blm-3.0';
export const userServiceUrl = `${authorizationUrl}/api/v1/user`;

// const LOGIN_CONSTANTS =  {
//   const LOGIN_PAGE = 'https://login-ppr.schneider-electric.com';
//   //var LOGIN_PAGE = 'https://login.dces.schneider-electric.com';
//   var HOST_URL = 'http://localhost:9000/';
//   return {
//     CLIENT_ID: 'blm-3.0',
//     GMR_CODE: '',
//     HOST_URL: HOST_URL,
//     LOGOUT_URL: '/oauth/disconnect',
//     USER_SERVICE_URL: LOGIN_PAGE + "/api/v1/user",
//     LOGIN_URL: '/authorize',
//     LOGIN_PAGE: LOGIN_PAGE,
//   };
// }

// export default LOGIN_CONSTANTS;
