const eOrchestratorUrl = 'https://elm-hannover.schneider-electric.com/eorchestrator/api/';
// API Gateway
// constant eOrchestratorUrl = 'https://gw-ppr-api.btsec.dev.schneider-electric.com/eorchestrator-ppr/eorchestrator/api/';
// Direct
// constant eOrchestratorUrl = 'https://ebroadcast-services-ppr.schneider-electric.com/eorchestrator/api/';
// Local
// constant eOrchestratorUrl = 'https://localhost:8086/api/';

const constants = {
  PUSHER_KEY: '061f329f8615d1de2c13',
  PUSHER_CHANNEL: 'presence-PLM-demoprod',
  PUSHER_CLUSTER: 'eu',
  GATEWAY_TOKEN: 'Bearer 1afa8f8275e8a33383571d32f7f9f514',
  MOCK: {
    taskService: {
      ENABLED: false,
      URL: '/data/tasks.json',
    },
    notificationService: {
      ENABLED: false,
      URL: '/data/notifications.json',
    },
  },
  eOrchestratorUrl,
};

export default constants;
