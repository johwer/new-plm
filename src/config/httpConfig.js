import Vue from 'vue';
import axios from 'axios';
import _ from 'underscore';
import constants from '../config/constConfig';
import loginStateService from '../services/loginStateService';

const self = this;

// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
Vue.prototype.$http = axios;

self.getHeader = (obj) => {
  const token = loginStateService.get();
  const gatewayToken = constants.GATEWAY_TOKEN;
  const headers = obj.headers || {};
  headers['Access-Control-Allow-Headers'] = headers['Access-Control-Allow-Headers'] || 'accept';
  const httpConfig = {
    headers,
  };

  if (_.isString(obj.url) && obj.url.indexOf('eorchestrator') >= 0 &&
    _.isString(gatewayToken) && gatewayToken.length > 0 && gatewayToken.indexOf('%%') < 0) {
    httpConfig.headers.Authorization = gatewayToken;
    httpConfig.headers.Authentication = token;
  } else if (!httpConfig.headers.Authorization) {
    httpConfig.headers.Authorization = token;
  }
  return httpConfig;
};

export default self;

