export function hashCode(str) {
  let hash = 0;
  if (!str || str.length === 0) {
    return hash;
  }
  for (let i = 0; i < str.length; i += 1) {
    const character = str.charCodeAt(i);
    /* eslint no-bitwise: 0 */
    hash = ((hash << 5) - hash) + character;
    /* eslint no-bitwise: 0 */
    hash &= hash; // Convert to 32bit integer
  }
  return hash;
}

function mod(n, m) {
  return ((n % m) + m) % m;
}

// TODO: could use caching here to save some computations
export function userColor(user, opacity) {
  const h = mod(hashCode(user.id), 360);
  const s = 75;
  const l = 75;
  const a = opacity || 1;
  return `hsla(${h}, ${s}%, ${l}%,  ${a})`;
}

export function stringToColor(string) {
  return this.userColor({ id: string });
}
