import axios from 'axios';
import _ from 'underscore';
import { userServiceUrl } from '@/config/loginConfig';
import constants from '@/config/constConfig';
import httpConfig from '@/config/httpConfig';

const paceUserByIdRest = `${constants.eOrchestratorUrl}{userIds}`;

export function addPaceUserFieldsToUser(paceUser, userInput) {
  let user = {};
  if (!_.isObject(userInput)) {
    user = {
      imsId: userInput,
      userId: userInput,
    };
  }
  if (!_.isEmpty(paceUser)) {
    user.id = paceUser.id;
    user.firstName = paceUser.firstName;
    user.lastName = paceUser.lastName;
    user.email = paceUser.email;
    user.companyName = paceUser.companyName;
    user.company = paceUser.companyName;
    user.jobFunction = paceUser.jobFunction;
    user.preferredLanguage = paceUser.preferredLanguage;
    user.country = paceUser.country;
    user.settings = paceUser.settings;
    user.avatarPresent = paceUser.avatarPresent;
  }
  return user;
}

function callPaceRestApi(userIds) {
  // if (!_.isArray(userIds) || userIds.length === 0) {
  //     var defer = $q.defer();
  //     defer.resolve([]);
  //     return defer.promise;
  // }

  /*
  return dataService.get({
      method: 'GET',
      url: paceUserByIdRest + userIds.join(','),
      unsetAccessControlAllowHeaders: true
  });
  */

  return axios.get(
    userServiceUrl,
    httpConfig.getHeader({ url: `${paceUserByIdRest}${userIds.join(',')}` }),
  );
}

// var putPaceUserToCache = function (paceUser){
//     if (!paceUserCache.get(paceUser.id)) {
//         paceUserCache.put(paceUser.id, paceUser);
//         // $log.debug('Adding user to cache' + paceUser.id);
//     } else {
//         // $log.debug('Adding user to cache but already found in cache! ' + paceUser.id);
//     }
// };

function addPaceInfoToUserList(arrayOfUsersInput, property) {
  const arrayOfUsers = arrayOfUsersInput.map(u => (_.isObject(u) ? u : { imsId: u }));
  const chain = _.chain(arrayOfUsers).pluck('imsId').uniq().filter(_.isString);
  const userIds = chain.value();
  const defer = new Promise();
  const paceUsersFromCache = [];
  const userIdsToGetFromPaceApi = [];
  userIds.forEach((userId) => {
    // const cachedUser = paceUserCache.get(userId);
    // if (cachedUser) {
    //   paceUsersFromCache.push(cachedUser);
    // $log.debug('Found user in cache ' + userId);
    // } else {
    userIdsToGetFromPaceApi.push(userId);
    // }
  });

  callPaceRestApi(userIdsToGetFromPaceApi).then((paceUsersFromPaceAPI) => {
    // _.each(paceUsersFromPaceAPI, putPaceUserToCache);

    const paceUsersCombined = _.union(paceUsersFromCache, paceUsersFromPaceAPI);
    arrayOfUsers.forEach((user) => {
      const paceUser = paceUsersCombined.find(p => p.id === user.imsId);
      addPaceUserFieldsToUser(paceUser, user);
    });
    defer.resolve(arrayOfUsers, property);
  }, (reason) => {
    defer.reject(reason);
  });
  return defer.promise;
}

function addPaceInfoToUser(user, property) {
  const defer = new Promise();
  addPaceInfoToUserList([user]).then(
    (data) => {
      defer.resolve(data[0], property);
    },
    (reason) => {
      defer.reject(reason);
    },
  );
  return defer.promise;
}

export function addPaceInfo(userOrArray, property) {
  if (_.isArray(userOrArray)) {
    return addPaceInfoToUserList(userOrArray, property);
  } else if (_.isObject(userOrArray)) {
    return addPaceInfoToUser(userOrArray, property);
  } else if (_.isString(userOrArray)) {
    return addPaceInfoToUser({ imsId: userOrArray }, property);
  }
  console.log('addPaceInfo does not have current type', userOrArray);
  const promise = new Promise();
  promise.reject(new Error(`addPaceInfo does not have current type:${userOrArray}`));
  return promise;
}

export function getUserProfile() {
  /*
  var token = sessionStorageService.get();
  var deferred = $q.defer();

  if (!token) {
      deferred.reject('No token present!');
  }
  var requestConf = {
      url: userServiceUrl,
      defaultHeadersReplace: {
          'Accept': 'application/json',
          'Authorization': token
      }
  };
  dataService.get(requestConf).then(
      function success(data) {
          deferred.resolve(data);
      }, function error(reason) {
          deferred.reject(reason);
      });
  return deferred.promise;
  */

  return axios.get(
    userServiceUrl,
    httpConfig.getHeader({ url: userServiceUrl }),
  );
}

