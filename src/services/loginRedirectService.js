import _ from 'underscore';
import loginStateService from '@/services/loginStateService';
import { authorizationUrl, clientId } from '@/config/loginConfig';

const self = this;
// const authorizationUrl = 'https://login-ppr.schneider-electric.com';
// const clientId = 'blm-3.0';

self.getTokenParamsFromUri = function getTokenParamsFromUri(uri) {
  const hash = uri.substr(1);
  const splitted = hash.split('&');
  const params = {};
  for (let i = 0; i < splitted.length; i += 1) {
    const param = splitted[i].split('=');
    const key = param[0];
    const value = param[1];
    params[key] = value;
  }
  return params.access_token ? params : undefined;
};

function getLoginUrl(redirectUriParam) {
  return `${authorizationUrl}/authorize?client_id=${clientId}&response_type=token&redirect_uri=${redirectUriParam}`;
}

self.refreshToken = function refreshToken(setTokenCallback, failureCallback) {
  console.log('Refresh token in iframe');
  const redirectUriParam = `${window.location.href}/refreshToken`;
  const url = getLoginUrl(redirectUriParam);

  // Due to CSM imperfections we decided to do this for refresh token
  const i = document.createElement('iframe');
  i.style.display = 'none';
  i.onload = function onload() {
    // console.log('iframe loaded', i);
    const { href } = i.contentWindow.location;
    // console.log('iframe url', href);
    const params = self.getTokenParamsFromUri(href.substr(href.indexOf('#')));
    // console.log('iframe params', params);
    i.parentNode.removeChild(i);

    if (!_.isEmpty(params) && !_.isEmpty(params.access_token)) {
      setTokenCallback(params);
    } else {
      console.warn('Refresh token failed', i);
      if (_.isFunction(failureCallback)) {
        failureCallback(10 * 1000);
      }
    }
  };
  i.src = url;
  document.body.appendChild(i);
};

self.login = function login(redirectUriParam) {
  // redirect to login
  let urlRedirectParm = '';
  if (window.location.hostname === 'localhost') {
    urlRedirectParm = `${window.location.protocol}//${window.location.hostname}:${window.location.port}`;
  } else {
    urlRedirectParm = `${window.location.protocol}//${window.location.hostname}`;
  }
  console.log(window.location.pathname);
  if (window.location.pathname === '/power-ai-landing/') {
    urlRedirectParm = `${urlRedirectParm}/power-ai`;
  }

  if (window.location.pathname === '/liquid-workforce-landing/') {
    console.log('Match');
    urlRedirectParm = `${urlRedirectParm}/liquid-workforce`;
  }

  if (window.location.pathname === '/exchange-industry-landing/') {
    console.log('Match');
    urlRedirectParm = `${urlRedirectParm}/exchange-industry`;
  }

  if (window.location.pathname === '/exchange-building-landing/') {
    urlRedirectParm = `${urlRedirectParm}/exchange-building`;
  }

  const redirectUri = redirectUriParam || urlRedirectParm;
  const url = getLoginUrl(redirectUri);
  console.log(url);
  window.location.replace(url);
};

self.logout = function logout(redirectUriParam) {
  loginStateService.unset(null);
  const redirectUri = redirectUriParam || window.location;
  const url = `${authorizationUrl}/oauth/disconnect?redirect=${redirectUri}`;
  window.sessionStorage.removeItem('token');
  window.location.replace(url);
};

export default self;
