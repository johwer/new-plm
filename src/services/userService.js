import axios from 'axios';
import constants from '../config/constConfig';
import httpConfig from '../config/httpConfig';

const restContactList = 'contactlist/';
const restMe = 'user/me';
const restUser = 'user/?userIds=';
const contactListRestEndpoint = `${constants.eOrchestratorUrl}${restContactList}`;
const meRestEndpoint = `${constants.eOrchestratorUrl}${restMe}`;
const userRestEndpoint = `${constants.eOrchestratorUrl}${restUser}`;

export function getContactList() {
  return axios.get(
    contactListRestEndpoint,
    httpConfig.getHeader({ url: contactListRestEndpoint }),
  );
}

export function getMe() {
  return axios.get(
    meRestEndpoint,
    httpConfig.getHeader({ url: meRestEndpoint }),
  );
}

export function getUsers(idArray) {
  // return Promise.resolve(`hej${idArray}`);
  // return Promise.resolve(idArray.map(i => ({ id: i, firstName: 'Pol', lastName: 'Pot' })));
  return axios.get(
    userRestEndpoint + idArray.join(','),
    httpConfig.getHeader({ url: userRestEndpoint }),
  );
}
