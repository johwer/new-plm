import _ from 'underscore';
import loginRedirectService from '@/services/loginRedirectService';

const self = this;
const redirectUrl = '/';
const expiresKey = 'token_expires';
const tokenKey = 'token';
const refreshToken = '/refreshToken';
let logoutTimer;
let refreshTimer;
let count = 0;

function clearTimers() {
  console.log('Reset token timers');
  window.clearTimeout(logoutTimer);
  window.clearTimeout(refreshTimer);
}

function resetExpires() {
  clearTimers();
  window.sessionStorage.removeItem(expiresKey);
  window.sessionStorage.removeItem(tokenKey);
}

function setLogoutTimer(millisToLogout) {
  console.log(`token expires timer in ${parseInt(millisToLogout / 1000, 10)} seconds`);
  logoutTimer = window.setTimeout(
    () => {
      console.log(`Token expired ${count}`);
      count += 1;
      resetExpires();
      loginRedirectService.logout(redirectUrl);
      // From modal trigger reload or directly go to activity
    },
    millisToLogout,
  );
}

self.setRefreshTimer = function setRefreshTimer(millisToRefreshParam) {
  const millisToRefresh = Math.max(millisToRefreshParam, 0);
  console.log(`refresh token in ${parseInt(millisToRefresh / 1000, 10)} seconds`);
  refreshTimer = window.setTimeout(
    () => {
      loginRedirectService.refreshToken(self.set, self.setRefreshTimer);
    },
    millisToRefresh,
  );
};

function startLoggedOutTimer(newTime) {
  if (newTime) {
    clearTimers();
    // console.log(`Set new token expires value millis ${newTime}`);
    window.sessionStorage.setItem(expiresKey, newTime);
  }

  const expiresMillis = window.sessionStorage.getItem(expiresKey);
  if (expiresMillis) {
    const now = new Date();
    if (expiresMillis < now.getTime()) {
      console.log('Found old token expires value. Deleting it');
      resetExpires();
    } else {
      const millisToLogout = (expiresMillis - now.getTime());
      // var millisToLogout = 60 * 1000;
      setLogoutTimer(millisToLogout);
      const millisToRefresh = millisToLogout - (2 * 60 * 1000);
      // const millisToRefresh = 5 * 1000;
      self.setRefreshTimer(millisToRefresh);
    }
  }
}

self.unset = () => {
  window.sessionStorage.removeItem(tokenKey);
  window.sessionStorage.removeItem(expiresKey);
};

self.set = function set(params) {
  console.log('Set token params', params);

  const accessToken = params.access_token;
  if (accessToken && accessToken.length !== 36) {
    throw new Error('Wrong format of access token', accessToken);
  }
  window.sessionStorage.setItem(tokenKey, accessToken);

  const expiresInSeconds = parseInt(params.expires_in, 10);
  const expires = new Date();
  expires.setSeconds(expires.getSeconds() + expiresInSeconds);

  startLoggedOutTimer(expires.getTime());
};

self.get = function get() {
  return window.sessionStorage.getItem(tokenKey);
};

self.getExpiresIn = function getExpiresIn() {
  const expiresMillis = window.sessionStorage.getItem(expiresKey);
  const millisToLogout = (expiresMillis - new Date().getTime());
  return millisToLogout / 1000;
};

self.isLoggedIn = () => !(_.isEmpty(self.get()));

self.init = function init() {
  if (window.location.href.indexOf(refreshToken) >= 0) {
    return;
  }
  startLoggedOutTimer();
};

export default self;
