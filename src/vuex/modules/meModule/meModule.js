import * as userService from '@/services/userService';

const actions = {
  setMe({ commit }, me) {
    commit('setMe', me);
  },
  unsetMe({ commit }) {
    commit('setMe', {});
  },
  fetchMe({ commit }) {
    userService.getMe().then(resp => commit('setMe', resp.data));
  },
};

const state = {
  me: {},
};

const getters = {
  getMe(state2) {
    return state2.me;
  },
};

const mutations = {
  setMe(theState, me) {
    state.me = me;
  },
};

const module = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};

export default module;
