import * as userService from '@/services/userService';

const actions = {
  async fetch({ commit }) {
    const contactList = (await userService.getContactList()).data.contacts;
    console.log('vuex contactList', contactList);
    const users = (await userService.getUsers(contactList)).data;
    console.log('vuex contacts', users);
    commit('set', users);
  },
};

const state = {
  contacts: [],
};

const getters = {
  get(theState) {
    return theState.contacts;
  },
  getIds(theState) {
    return theState.contacts.map(u => u.id);
  },
};

const mutations = {
  set(theState, contacts) {
    state.contacts = contacts;
  },
};

const module = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};

export default module;
