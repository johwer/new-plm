import Vue from 'vue';
import Vuex from 'vuex';
// Separate Module States
import meModule from '../vuex/modules/meModule/meModule';
import contacts from '../vuex/modules/contacts/contacts';

Vue.use(Vuex);

// Import modules
export default new Vuex.Store({
  modules: {
    meModule,
    contacts,
  },
});
