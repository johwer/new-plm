// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import App from './App';
import router from './router';
import './config/httpConfig';
import './config/gettextConfig';
import store from './vuex/vuexConfig';

Vue.config.productionTip = false;
Vue.use(VueMaterial);

// const success = (response) => { console.log('SMHI data: ', response); };
// const failure = (e) => { console.log(e); };
// Vue.prototype.$http.get('https://opendata-download-warnings.smhi.se/api/version/2/alerts.json').then(success, failure);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  store,
});
