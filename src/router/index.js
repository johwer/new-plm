import Vue from 'vue';
import Router from 'vue-router';
import Landing from '@/pages/Landing';
import ExchangeBuildingLanding from '@/pages/ExchangeBuildingLanding';
import ExchangeBuilding from '@/pages/ExchangeBuilding';
import ExchangeIndustryLanding from '@/pages/ExchangeIndustryLanding';
import ExchangeIndustry from '@/pages/ExchangeIndustry';
import ExchangePowerLanding from '@/pages/ExchangePowerLanding';
import LiquidWorkforceLanding from '@/pages/LiquidWorkforceLanding';
import LiquidWorkforce from '@/pages/LiquidWorkforce';
// import OpenEcosystemCollaborationLanding from '@/pages/OpenEcosystemCollaborationLanding';
import PowerAILanding from '@/pages/PowerAILanding';
import PowerAI from '@/pages/PowerAI';
import HelloWorld from '@/pages/HelloWorld';
import PlmCollborate from '@/pages/PlmCollborate';
import loginRedirectService from '@/services/loginRedirectService';
import loginStateService from '@/services/loginStateService';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/landing',
      name: 'Landing',
      component: Landing,
      meta: { product: 'landing' },
    },
    {
      path: '/exchange-industry-landing',
      name: 'ExchangeIndustryLanding',
      component: ExchangeIndustryLanding,
      meta: { product: 'exchange-industry-landing' },
    },
    {
      path: '/exchange-industry',
      name: 'ExchangeIndustry',
      component: ExchangeIndustry,
      meta: { product: 'exchange-industry' },
      beforeEnter: (to, from, next) => {
        if (loginStateService.isLoggedIn()) {
          next();
        } else {
          next('/exchange-industry-landing');
        }
      },
    },
    {
      path: '/exchange-building-landing',
      name: 'ExchangeBuildingLanding',
      component: ExchangeBuildingLanding,
      meta: { product: 'exchange-building-landing' },
    },
    {
      path: '/exchange-building',
      name: 'ExchangeBuilding',
      component: ExchangeBuilding,
      meta: { product: 'exchange-building' },
      beforeEnter: (to, from, next) => {
        if (loginStateService.isLoggedIn()) {
          next();
        } else {
          next('/exchange-building-landing');
        }
      },
    },
    {
      path: '/liquid-workforce-landing',
      name: 'LiquidWorkforceLanding',
      component: LiquidWorkforceLanding,
      meta: { product: 'liquid-workforce-landing' },
    },
    {
      path: '/liquid-workforce',
      name: 'LiquidWorkforce',
      component: LiquidWorkforce,
      meta: { product: 'liquid-workforce' },
      beforeEnter: (to, from, next) => {
        if (loginStateService.isLoggedIn()) {
          next();
        } else {
          next('/liquid-workforce-landing');
        }
      },
    },
    {
      path: '/power-ai-landing',
      name: 'PowerAILanding',
      component: PowerAILanding,
      meta: { product: 'power-ai-landing' },
    },
    {
      path: '/power-ai',
      name: 'PowerAI',
      component: PowerAI,
      meta: { product: 'power-ai' },
      beforeEnter: (to, from, next) => {
        if (loginStateService.isLoggedIn()) {
          next();
        } else {
          next('/power-ai-landing');
        }
      },
    },
    {
      path: '/exchange-power-landing/',
      name: 'ExchangePowerLanding',
      component: ExchangePowerLanding,
      meta: { product: 'exchange-power-landing' },
    },
    {
      path: '/landing/test',
      name: 'Hello',
      component: HelloWorld,
      meta: { product: 'landing' },
    },
    {
      path: '/portal/collaborate',
      name: 'PlmCollborate',
      component: PlmCollborate,
      meta: { product: 'portal' },
    },
    {
      path: '/external/plm',
      beforeEnter: (to, from, next) => {
        console.log('Before', to, from, next);

        let loginParams = '';
        if (loginStateService.isLoggedIn()) {
          loginParams = `#access_token=${loginStateService.get()}&expires_in=${loginStateService.getExpiresIn()}`;
        }
        console.log('loginParams', loginParams);
        window.location.href = `https://elm.schneider-electric.com/${loginParams}`;
      },
      beforeRouteUpdate() {
        console.log('update');
      },
    },
    {
      path: '*',
      redirect: '/landing',
      beforeEnter: (to) => {
        console.log('Before');
        console.log(to);
        // window.location.href('https://elm.schneider-electric.com/');
      },
      beforeRouteUpdate() {
        console.log('update');
      },
    },
  ],
});

const handleReturnFromLoginRedirect = (to, from, next) => {
  const paramsFromUri = loginRedirectService.getTokenParamsFromUri(window.location.hash);
  if (paramsFromUri) {
    // its a login attempt
    // refresh token, don't continue to page!
    if (window.location.href.indexOf('/refreshToken') >= 0) {
      return;
    }
    // normal login, redirect to the same path, without login hash
    loginStateService.set(paramsFromUri);
    window.location.replace(window.location.href.substr(0, window.location.href.indexOf('#')));
  } else {
    next();
  }
};
router.beforeEach(handleReturnFromLoginRedirect);

export default router;
